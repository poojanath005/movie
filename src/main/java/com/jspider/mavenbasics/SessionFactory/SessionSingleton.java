package com.jspider.mavenbasics.SessionFactory;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

class SessionSingletonEx{
	
	private static SessionFactory sf = null;
	
	public static SessionFactory getInstance() 
	{
			
			if(sf == null) 
			{	
				Configuration cfg = new Configuration();
				cfg.configure();
				SessionFactory sessionFactory = cfg.buildSessionFactory();
				sf = sessionFactory;			
			}	
		    return sf;
	}
}



	


