package com.jspiders.mavenbasics.repository;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import com.jspiders.mavenbasics.entity.Movie;


public class MovieRepository {
	
	public void saveMovieDetails(Movie movie) {
		try {
			Configuration cfg1 = new Configuration();
			cfg1.configure();
			SessionFactory sessionFactory1 = cfg1.buildSessionFactory();
			Session session = sessionFactory1.openSession();
		Transaction transaction = session.beginTransaction();
		session.save(movie);
		transaction.commit();
		}
		catch(HibernateException e) {
			
		}
		
	}
	public Movie getMovieById(Long id) {

//		Configuration cfg1 = new Configuration();
//		cfg1.configure();
//		SessionFactory sessionFactory1 = cfg1.buildSessionFactory();
//		Session session1 = sessionFactory1.openSession();
		Session session1= (SessionSingletonEx.SessionFactory()).openSession();
		return session1.get(Movie.class, id);
		
	}
	public void updateRatingAndBudget(String rating, Double budget, Long id) {
		
		Movie m = getMovieById(id);
		if(m!=null) {
			m.setRating(rating);
			m.setBudget(budget);
		
		Configuration cfg = new Configuration();
		cfg.configure();
		SessionFactory sessionFactory = cfg.buildSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		session.merge(m);
		transaction.commit();
		}
//		else {
//			System.out.println("Updation failed");
//		}

		
	}
	
	public List<Movie> findAll(){
		Configuration cfg = new Configuration();
		cfg.configure();
		SessionFactory sessionFactory = cfg.buildSessionFactory();
		Session session = sessionFactory.openSession();
		String hql ="from Movie";
		Query query = session.createQuery(hql);
		return query.list();

   }
	
	public Movie findByName(String movieName) {
		Configuration cfg = new Configuration();
		cfg.configure();
		SessionFactory sessionFactory = cfg.buildSessionFactory();
		Session session = sessionFactory.openSession();
		String hql = "from Movie where name=:mName";
		Query query = session.createQuery(hql);
		return (Movie) query.uniqueResult();
	}
	
	public void updateRatingAndBudgetByName(String name, String rating, Double budget) {
		Configuration cfg = new Configuration();
		cfg.configure();
		SessionFactory sessionFactory = cfg.buildSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		String hql ="update Movie set rating=:r, budget=:b where name=:m";
		Query query = session.createQuery(hql);
		query.setParameter("r", rating);
		query.setParameter("b", budget);
		query.setParameter("m", name);
		int rowsUpdated = query.executeUpdate();
		transaction.commit();
		if(rowsUpdated > 0)
		{
			System.out.println("Update successful");
		}
		else
		{
			System.out.println("Update Failed");
			
		}
		
	}
	
	public void deleteByName(String name) {
		
		Configuration cfg = new Configuration();
		cfg.configure();
		SessionFactory sessionFactory = cfg.buildSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		String hql ="delete from Movie where name=:m";
		Query query = session.createQuery(hql);
	    query.setParameter("m", name);
		
		int rowsUpdated = query.executeUpdate();
	
		transaction.commit();
		if(rowsUpdated > 0)
		{
			System.out.println("Deleted successful");
		}
		else
		{
			System.out.println("Failed to Delete");
			
		}
		
	}
}

