package com.jspiders.mavenbasics;

import java.util.Date;
import java.util.List;

import com.jspiders.mavenbasics.entity.Movie;
import com.jspiders.mavenbasics.repository.MovieRepository;


public class App 
{
    public static void main( String[] args )
    {
       Movie movie = new Movie();
       movie.setId(156L);
       movie.setName("Love And Thunder");
       movie.setBudget(100D);
       movie.setRating("5");
       movie.setReleaseDate(new Date());
       
       
       MovieRepository movieRepository = new MovieRepository();
      
      movieRepository.saveMovieDetails(movie);
//       
//       System.out.println(movieRepository.getMovieById(123L));
//       
//       movieRepository.updateRatingAndBudget("5", 800.00, 143L);
//       movieRepository.saveMovieDetails(movie1);
//       List<Movie> movieList = movieRepository.findAll();
//       movieList.forEach(each->{
//    	   System.out.println(each);
//       });
       
       //movieRepository.findByName("THOR");
       //movieRepository.updateRatingAndBudgetByName("THOR", "4.5", 666000D);
     // movieRepository.deleteByName("THOR");
    }
}
